import pygame 

pygame.init()

Ancho= 500
Alto = 500

Blanco= (255, 255, 255)
Negro= (0,0,0)
Azul=(0,255,255)

screen= pygame.display.set_mode((Ancho,Alto))

pos_x=250
pos_y=250
running=True
while running:
  
    for event in pygame.event.get():
        if event.type==pygame.QUIT:
            running=False
        if event.type==pygame.KEYDOWN:
            if event.key==pygame.K_ESCAPE:
                running=False
            if event.key == pygame.K_RIGHT:
                pos_y+=10 
            if event.key == pygame.K_LEFT:
                pos_y-=10
            if event.key == pygame.K_DOWN:
                pos_y+=10
            if event.key == pygame.K_UP:
                pos_y-=10 
            
    screen.fill(Azul)            

    pygame.draw.circle(screen,Blanco, ((pos_x,pos_y)), 50)

    pygame.display.update()
 
    
pygame.quit() 


